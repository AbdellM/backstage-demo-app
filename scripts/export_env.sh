#!/bin/bash

# Load and export environment variables from .env file
set -a  # Automatically export all variables
source .env.local
set +a  # Disable auto export